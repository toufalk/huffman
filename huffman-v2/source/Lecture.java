import java.io.FileInputStream;
import java.io.IOException;

public class Lecture {

    private String nomfic;
    private FileInputStream lecteur;
    private int buff;
    private int n;


    public Lecture(String filename) {
        nomfic = filename;
        reset();
    }

    public int lireOctet() {
        int rep = 0;
        for (int k=0; k<8; k++) {
            rep = rep << 1;
            int bitlu = lireBit();
            if (bitlu == -1) return -1;
            rep = rep | bitlu;
        }
        return rep;
    }

    public int lireBit() {
        try {
            if (n == -1) {
                buff = lecteur.read();
                n = 7;
            }
        } catch (IOException ioe) { buff = -1; }
        if (buff == -1) return -1;
        int rep = (buff & (1<<n)) >> n;
        n--;
        return rep;
    }


    public void reset() {
        try {
            lecteur.close();
        } catch(Exception e) {}
        try {
            lecteur = new FileInputStream(nomfic);
        } catch(Exception e) {
            e.printStackTrace(System.err);
            System.exit(1);
        }
        n = -1;
    }

}
