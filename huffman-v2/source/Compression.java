
/**
 * Coeur de la compression de Huffman.
 * <p>
 * Cette classe ne dispose que de méthodes statiques (pas d'objet).
 * Elle propose la compression selon Huffman en version statique (la version).
 * La version dynamique de la méthode est prévue.
 * </p>
 * 
 *
 */
public class Compression {


    /**
     * Compresse le fichier par la méthode statique de Huffman.
     * <p>
     * On calcule les fréquences d'apparition des octets puis 
     * l'arbre de Huffman et enfin la table de correspondance.
     * Il suffit alors de reparcourir la source pour compresser
     * chaque octet.
     * </p> 
     *
     * @param source
     *        le fichier d'origine à compresser
     * @param occurrences
     *        le fichier dans lequel stocker l'arbre
     * @param resultat
     *        le fichier dans lequel stocker les données
     */
    public static void statique(Lecture source, Ecriture arbreFic, Ecriture resultat) {
        // calcul du tableau des frequences
        int[] freq = new int[256];
        int octet = source.lireOctet();
        while (octet != -1) {
            freq[octet]++;
            octet = source.lireOctet();
        }
        
        // construction de l'arbre et enregistrement puis la table de correspondance
        ArbreH arbre = construitArbre(freq, true, false);
        stockArbre(arbre, arbreFic, -1);
        //System.out.println(arbre);
        if (arbreFic != resultat) arbreFic.close();
        String[] tabcod = genereCorrespondances(arbre,"", new String[257]);
        
        // boucle de compression
        source.reset();
        octet = source.lireOctet();
        while (octet != -1) {
            resultat.ecrireBit( tabcod[octet] );
            octet = source.lireOctet();
        }
        // fin d'écriture
        resultat.ecrireBit( tabcod[FeuilleH.EOF_LABEL] );
        resultat.close();
    }



    /**
     * Compression dynamique de Huffman.
     * <p>
     * On ajoute 2 octets virtuels, 256 et 257. Ces octets
     * signifient respectivement une nouvelle feuille dans
     * l'arbre et une fin de fichier.
     * </p>
     * <p>
     * Le fichier source est parcouru une seule fois.
     * Initialement, il n'y a que les octets virtuels dans l'arbre.
     * A chaque lecture d'octet, on le code, puis on met à jour la table
     * de fréquence et l'arbre de Huffman. </br>
     * Si l'octet n'est pas déjà dans l'arbre, on envoie le code de l'octet
     * virtuel 257 (nouvelle feuille) puis l'octet lu. Sinon, on envoie simplement
     * son code. En fin de fichier, on envoie le code de l'octet virtuel 256.
     * </p>
     *
     * @param source
     *        le fichier à compresser
     * @param compresse
     *        le fichier compresser à écrire
     */
    public static void dynamique(Lecture source, Ecriture compresse) {
        // on prendra comme convention : 257 -> nouvelle feuille ; 256 -> fin de fichier
        int[] freq = new int[258];
        ArbreH arbre = construitArbre(freq, true, true);
        String[] tabcod = genereCorrespondances(arbre,"", new String[258]);
        int octet = source.lireOctet();
        while (octet != -1) {
            if ( freq[octet] > 0) {
                compresse.ecrireBit( tabcod[octet] );
            } else {
                compresse.ecrireBit( tabcod[FeuilleH.NOC_LABEL] );
                compresse.ecrireOctet( octet );
            }
            freq[octet]++;
            arbre = construitArbre(freq, true, true);
            tabcod = genereCorrespondances(arbre,"", new String[258]);
            octet = source.lireOctet();
        }
        // fin d'écriture
        compresse.ecrireBit( tabcod[FeuilleH.EOF_LABEL] );
        compresse.close();
    }

    

    /*
     * méthodes privées
     */
    
    // @param freq: tableau des fréquences (nb occurrences)
    public static ArbreH construitArbre(int[] freq, boolean EOFLeaf, boolean newOctetLeaf) {
        // initialisation de la FAP
        FAP hf = new FAP();
        for (int k=0; k<freq.length; k++) {
            if (freq[k] > 0) {
                hf.inserer(new ElementFAP(new FeuilleH(k), -freq[k]));
                //System.out.println((char)k + " apparait " + freq[k] + " fois.");
            }
        }
        // ajout des feuilles virtuelles
        if (EOFLeaf) hf.inserer(new ElementFAP(new FeuilleH(FeuilleH.EOF_LABEL, true), 0));
        if (newOctetLeaf) hf.inserer(new ElementFAP(new FeuilleH(FeuilleH.NOC_LABEL), 0));
        
        
        // construction de l'arbre
        while (hf.taille() > 1) {
            ElementFAP elem1 = hf.extraire();
            ElementFAP elem2 = hf.extraire();
            if (elem1.arbre().toujoursADroite()) {
                ElementFAP tmp = elem1;
                elem1 = elem2;
                elem2 = tmp;
            }
            hf.inserer(new ElementFAP(new NoeudH(elem1.arbre(), elem2.arbre(), elem2.arbre().toujoursADroite()),
                                      elem1.priorite() + elem2.priorite()));
        }
        ArbreH arbre = hf.extraire().arbre();
        //System.out.println(arbre);
        return arbre;
    }
    
    

    private static int stockArbre(ArbreH arbre, Ecriture occ, int virtEOF) {
        /*
         * méthode pour appeler la méthode récursive de stockage de l'arbre dans un fichier.
         *
         * fichier : 1 octet pour le nombre de feuilles total
         * puis, 2 octets par feuilles : son étiquette + sa profondeur dans l'arbre.
         *
         * On parcours l'arbre "de gauche à droite" et on ajoute de la même façon.
         */
        int virt = virtEOF;
        if (arbre.estFeuille()) {
            occ.ecrireBit(1);
            if (arbre.etiquette() == FeuilleH.EOF_LABEL) {
                occ.ecrireOctet(virtEOF);
            } else {
                occ.ecrireOctet(arbre.etiquette());
                if (virt < 0) virt = arbre.etiquette();
            }
        } else {
            occ.ecrireBit(0);
            virt = stockArbre(arbre.filsGauche(), occ, virt);
            virt = stockArbre(arbre.filsDroit(), occ, virt);
        }
        return virt;
    }
    
    
    
    
    /*
     * Méthode récursive pour générer la table de correspondance.
     */
    private static String[] genereCorrespondances(ArbreH arbre, String code, String[] tabcod){
        if (arbre.estFeuille()){
            tabcod[arbre.etiquette()] = code;
        } else {
            tabcod = genereCorrespondances(arbre.filsGauche(), code + '0', tabcod);
            tabcod = genereCorrespondances(arbre.filsDroit(), code + '1', tabcod);
        }
        return tabcod;
    }


    public static void main(String[] arg) {
        Lecture l = new Lecture("test/barbara");
        Ecriture e = new Ecriture("toto.tree");
        Ecriture r = new Ecriture("toto.comp");
        //statique(l, r, r);
        dynamique(l,r);
    }
    
}
