
/**
 * Une feuille de l'arbre de Huffman.
 * <p>
 * Elle stocke l'étiquette, c'est-à-dire l'octet (le caractère) représenté sous forme d'int.
 * Elle implémente bien sûr l'interface ArbreH
 * </p>
 *
 */
public class FeuilleH implements ArbreH {

    final public static int EOF_LABEL = 256;
    final public static int NOC_LABEL = 257;

  
    private int et;
    private boolean adroite;

    /**
     * Construction d'une feuille à partir de son étiquette.
     *
     * @param etiq
     *        l'étiquette de la feuille
     */
    public FeuilleH(int etiq) {
        this(etiq, false);
    }


    public FeuilleH(int etiq, boolean toujoursADroite) {
        et = etiq;
        adroite = toujoursADroite;
    }

    
    /*
     * Méthodes de l'interface ArbreH
     */

    /**
     * @return Vrai.
     */
    public boolean estFeuille() {
        return true;
    }

    /**
     * Lève une exception.
     */
    public ArbreH filsGauche() throws RuntimeException {
       throw new RuntimeException("Une feuille n'a pas de fils gauche...");
    }

    /**
     * Lève une exception.
     */
    public ArbreH filsDroit() throws RuntimeException {
         throw new RuntimeException("Une feuille n'a pas de fils droit...");
    }

    public int etiquette() throws RuntimeException {
        return et;
    }

    public boolean toujoursADroite() {
        return adroite;
    }


    /*
     * Méthodes pur affichage correct dans un terminal
     */
    public String toString(String prefixe, boolean adroite) {
        if (etiquette() == EOF_LABEL) return prefixe + "+-- EOF\n";
        if (etiquette() == NOC_LABEL) return prefixe + "+-- NOC\n";
        return prefixe + "+-- " + (char)etiquette() + "\n";
    }

    public String toString() {
        return toString("", true);
    }
    

}

    
