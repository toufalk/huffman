
/**
 * File à priorité nécessaire pour construire l'arbre de Huffman.
 *
 */
public class FAP {

    
    private ElementFAP[] tab;
    
    // nombre d'éléments actuel dans la FAP.
    private int taille;

    
    /**
     * Construit une FAP vide.
     */
    public FAP() { 
        tab = new ElementFAP[258]; // il faut au maximum 256 octets + 2 octets virtuels (en dynamique).
        taille = 0;
    }


    
    /**
     * Donne le nombre d'éléments dans la file.
     *
     * @return le nombre d'éléments dans la file
     */
    public int taille() {
	return taille;
    }


    /**
     * Insère l'élément à sa place dans la FAP.
     *
     * @param element
     *        l'élément à insérer dans la file
     */
    public void inserer(ElementFAP element) {
        tab[taille] = element;
        taille++;
        percolerHaut(taille-1);
    }


    /**
     * Enlève et retourne l'élément de priorité maximale de la FAP.
     *
     * @return l'élément de la file de plus haute priorité.
     *
     * @throws IndexOutOfBoundsException si on tente d'extraire un élément d'une FAP vide.
     */
    public ElementFAP extraire() {
	if (taille == 0) throw new IndexOutOfBoundsException("FAP vide");
	ElementFAP tmp = tab[0];
        if (taille > 1) {
            tab[0] = tab[taille-1];
            taille--;
            percolerBas(0);
        } else {
            taille = 0;
        }
        return tmp;
    }



    /**
     * Affichage du contenu de la FAP.
     *
     * @return une chaine représentant l'état de la FAP.
     */
    public String toString() {
        String txt = "Taille de la FAP : " + taille;
        for (int k=0; k<taille; k++) {
            txt += tab[k] + "\n";
        }
        return txt;
    }





    /*
     * Méthodes privées
     */

    // Indice du fils droit
    private int filsDroit(int i) throws Exception {
        int fils = 2*i + 2;
        if (fils >= taille) throw new Exception("no child found.");
        return fils;
    }

    // Indice du fils gauche
    private int filsGauche(int i) throws Exception {
        int fils = 2*i + 1;
        if (fils >= taille) throw new Exception("no child found.");
        return fils;
    }

    // Indice du père
    private int pere(int i) throws Exception {
        if (i == 0) throw new Exception("no parent found.");
        return (i-1)/2;
    }

    // Fait remonter un élément dans la FAP s'il est plus prioritaire.
    private void percolerHaut(int i) {
        boolean fini = false;
        int indice = i;
        try {
            while (!fini) {
                int papa = pere(indice);
                if (tab[indice].estPlusPrioritaire(tab[papa])) {
                    ElementFAP tmp = tab[papa];
                    tab[papa] = tab[indice];
                    tab[indice] = tmp;
                    indice = papa;
                } else {
                    fini = true;
                }
            }
        } catch (Exception e) {}
    }

    // Indice du plus grand fils, s'il existe
    private int maxFils(int i) throws Exception {
        int indice = filsGauche(i);
        ElementFAP maxi = tab[indice];
        try {
            int indiceDroit = filsDroit(i);
            ElementFAP droit = tab[indiceDroit];
            if (tab[indiceDroit].estPlusPrioritaire(maxi)) {
                indice = indiceDroit;
            }
        } catch (Exception e) {}
        return indice;
    }

    // Fait redescendre un élément dans la FAP s'il est moins prioritaire.
    private void percolerBas(int i) {
        boolean fini = false;
        int indice = i;
        try {
            while (!fini) {
                int fiston = maxFils(indice);
                if (tab[fiston].estPlusPrioritaire(tab[indice])) {
                    ElementFAP tmp = tab[fiston];
                    tab[fiston] = tab[indice];
                    tab[indice] = tmp;
                    indice = fiston;
                } else {
                    fini = true;
                }
            }
        } catch (Exception e) {}
    }


    
}
