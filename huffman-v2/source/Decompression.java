import java.util.LinkedList;

/**
 * Coeur de la décompression de Huffman.
 * <p>
 * Cette classe ne dispose que de méthodes statiques (pas d'objet).
 * Elle propose la decompression selon Huffman en version statique (la version).
 * La version dynamique de la méthode est prévue.
 * </p>
 *
 */
public class Decompression {

   
    /**
     * Décompresse le fichier par la méthode statique de Huffman
     * <p>
     * On commence par lire l'arbre puis on lit bit à bit le fichier compréssé
     * et on se déplace dans l'arbre de Huffman, 0 à gauche, 1 à droite.
     * Quand on tombe sur une feuille, on écrit le caractère correspondant à l'étiquette dans le fichier de sortie
     * et on se replace à la racine.
     * On itère le processus jusqu'à la fin du fichier compressé.
     * Enfin, on sauve le fichier décompressé.
     * </p>
     *
     * @param occurrence
     *        le fichier où se trouve l'arbre à lire
     * @param compresse
     *        le fichier compréssé à lire pour décodage
     * @param sortie
     *        le fichier dans lequel on écrit le texte décodé
     *
     * @see ArbreH
     */
    public static void statique(Lecture arbreFic, Lecture compresse, Ecriture sortie) {
        ArbreH racine = destockArbre(arbreFic, new ElementFAP(null, -1));
        System.out.println(racine);
        ArbreH noeud = racine;
        int bit = compresse.lireBit();
        while (bit != -1) {
            noeud = (bit == 0 ? noeud.filsGauche() : noeud.filsDroit());
            if (noeud.estFeuille()) {
                if (noeud.etiquette() == FeuilleH.EOF_LABEL) break;
                sortie.ecrireOctet(noeud.etiquette());
                noeud = racine;
            }
            bit = compresse.lireBit();
        }
        // fin de décompression
        sortie.close();
    }



    public static void dynamique(Lecture compress, Ecriture decompress) {
        // on prendra comme convention : 257 -> nouvelle feuille ; 256 -> fin de fichier
        int[] freq = new int[258];
        ArbreH noeud = Compression.construitArbre(freq, true, true);
        int bit = compress.lireBit();
        while (bit != -1) {
            noeud = (bit == 0 ? noeud.filsGauche() : noeud.filsDroit());
            if (noeud.estFeuille()) {
                int octet = noeud.etiquette();
                if (octet == FeuilleH.EOF_LABEL) break;
                if (octet == FeuilleH.NOC_LABEL) octet = compress.lireOctet();
                decompress.ecrireOctet(octet);
                freq[octet]++;
                noeud = Compression.construitArbre(freq, true, true);
            }
            bit = compress.lireBit();
        }
        // fin d'écriture
        decompress.close();
    }




    /*
    private static ArbreH destockArbre(Lecture occ) {
        ElementFAP elem = destockArbre(occ, new ElementFAP(null, -1));
        return elem.arbre();
    }

    */

    private static ArbreH destockArbre(Lecture occ, ElementFAP elem) {
        int bit = occ.lireBit();
        if (bit == 1) {
            int octet = occ.lireOctet();
            if (octet == elem.priorite()) return new FeuilleH(FeuilleH.EOF_LABEL);
            if (elem.priorite() < 0) elem.setPriorite(octet);
            return new FeuilleH(octet);
        } else {
            return new NoeudH(destockArbre(occ, elem), destockArbre(occ, elem));
        }
    }


    /*
    private static ArbreH destockageArbre(int profondeur, LinkedList<ElementFAP> liste) {
        /*
         * méthode récursive de reconstruction.
         *
         * Si on est à la bonne profondeur, on crée un feuille
         * sinon, on crée un noeud interne qu'on rempli, d'abord à gauche, puis à droite
         * en vidant progressivement la file.
         *
        if (profondeur == liste.getFirst().prio) {
            return liste.poll().arbre;
        }
        NoeudH ne = new NoeudH(null, null);
        ne.setFilsGauche(destockageArbre(profondeur+1, liste));
        ne.setFilsDroit(destockageArbre(profondeur+1, liste));
        return ne;
        
        return null;
    }
        */


    public static void main(String[] arg) {
        Lecture la = new Lecture("toto.tree");
        Lecture lc = new Lecture("toto.comp");
        Ecriture e = new Ecriture("toto.decomp");
        statique(lc, lc, e);
        //dynamique(lc, e);
    }


}
