
/**
 * interface pour l'arbre de Huffman.
 * <p>
 * Cette interface est destinée à être implémentée par les noeuds et les feuilles.
 * Ces classes doivent implémenter TOUTES les méthodes de l'interface.
 * Si une méthode n'est pas utile pour le contexte, elle doit lancer une RuntimeException.
 * </p>
 *
 */
public interface ArbreH {


    /**
     * L'arbre est-il une simple feuille ?
     *
     * @return vrai si c'est une feuille, faux sinon.
     */
    public boolean estFeuille();
    

    /**
     * retourne le fils gauche du noeud.
     *
     * @return le fils gauche de l'arbre
     *
     * @throws RuntimeException si on cherche le fils gauche d'une feuille.
     */
    public ArbreH filsGauche() throws RuntimeException;


    /**
     * retourne le fils droit du noeud.
     *
     * @return le fils droit de l'arbre
     *
     * @throws RuntimeException si on cherche le fils droit d'une feuille.
     */
    public ArbreH filsDroit() throws RuntimeException;


    /**
     * retourne l'étiquette de la feuille.
     *
     * @return l'etiquette de la racine
     *
     * @throws RuntimeException si on cherche l'etiquette d'un noeud interne.
     */
    public int etiquette() throws RuntimeException;


    /**
     * indique si le noeud doit nécésseaisement être placé à droite dans un sur-arbre.
     *
     * @return vrai si cet arbre doit nécéssairemetn être placé dans un fils droit.
     */
    public boolean toujoursADroite();



    /**
     * Affichage de l'arbre.
     *
     * @param prefixe
     *        un prefixe à ajouter à chaque début de ligne de l'arbre.
     * @param adroite
     *        Affiche-t-on la partie droite d'un sur-arbre ?
     *
     * @return une chaine représentant l'arbre.
     */
    public String toString(String prefixe, boolean adroite);
    
}
