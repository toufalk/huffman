import java.io.PrintStream;


public class Ecriture {


    private PrintStream scripteur;
    private int buff;
    private int n; // position d'ecriture dans le buffer /buff/


    /**
     * Création d'un objet Ecritue pour écrire dans un fichier.
     *
     * @param filename
     *        le nom du fichier à écrire
     */
    public Ecriture(String filename) {
        buff = 0;
        n = 7;
        try {
            scripteur = new PrintStream(filename);
        } catch(Exception e) {
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }


    /**
     * Ecrire un octet dans le fichier.
     *
     * Tronque l'entier <i>octet</i> aux 8 premiers bits.
     *
     * @param octet
     *        l'octet à écrire
     */
    public void ecrireOctet(int octet) {
        for (int k=7; k>=0; k--) {
            ecrireBit(octet>>k);
        }
    }


    public void ecrireOctet(String str) {
        for (int k=0; k<str.length(); k++)
            ecrireOctet((char)str.charAt(k));
    }

    /**
     * Ecrire un bit dans le fichier
     *
     * Tronque l'entier <i>bit</i> à son premier bit :
     * Si l'entier est pair, on écrit le bit 0,
     * s'il est impair, on écrit le bit 1.
     *
     * @param bit
     *        le bit à écrire
     */
    public void ecrireBit(int bit) {
        buff = buff | ((bit & 1) * (1<<n));
        n--;
        if (n == -1) {// octet plein, on archive et on prépare le suivant.
            scripteur.write(buff);
            buff = 0;
            n = 7;
        }
    }


    public void ecrireBit(String str) {
        for (int k=0; k<str.length(); k++)
            ecrireBit((char)str.charAt(k));
    }


    public void close() {
        if (n < 7) {
            scripteur.write(buff);
            scripteur.close();
        }
    }


}
