/**
 * Interface d'écriture dans un fichier.
 *
 * @author Vincent
 */
public interface Ecriture {

    /**
     * Écriture dans le fichier.
     *
     * @param data
     *        la donnée à écrire.
     */
    public void ecrire(int data);

    /**
     * Écriture dans le fichier.
     *
     * @param text
     *        la chaine à écrire.
     */
    public void ecrire(String text);

    /**
     * Indique la fin de l'écriture.
     * <p>
     * Toutes les opérations de post-écritures doivent être faites dans
     * cette méthode.
     * Si l'Ecriture utilise des buffers, c'est le moment de les vider.
     * </p>
     */
    public void flush();
}
