class EquilPeigne {

    public static void main(String[] arg) {
        EcritureOctet eo = new EcritureOctet("equilibre");
        for (int n=0; n<10000; n++) {
            System.out.println(n);
            for (int k=0; k<256; k++) {
                eo.ecrire(k);
            }
        }
        eo.flush();

        eo = new EcritureOctet("peigne");
        int n = 1;
        int k = 0;
        while (k < 24) {
            System.out.println(k);
            for (int i=0; i<n; i++) {
                eo.ecrire('a' + k);
            }
            k++;
            n += n;
        }
        eo.flush();
    }
    
}
