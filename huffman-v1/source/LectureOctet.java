import java.io.InputStream;
import java.io.FileInputStream;

/**
 * Lecture de fichiers.
 *
 * Lit dans le fichier de donnée, octet par octet.
 *
 * @author Vincent
 *
 */
public class LectureOctet implements Lecture {


    private InputStream lecteur;
    private String nom;
    
    // bitMode == true : le caractère '0' représente le bit 0,
    // bitMode == false: le caractère '0' représente l'octet 48 (ASCII)
    private boolean bitMode;
        

    /**
     * Crée un flux de lecture de fichier par octet.
     *
     * @param nomFichier
     *        le nom du fichier à lire
     */
    public LectureOctet(String nomFichier) {
        bitMode = false;
        nom = nomFichier;
        retourDebut();
    }



    /**
     * Lecture du prochain octet du fichier.
     *
     * @return le prochain octet du fichier ou -1 s'il n'y a plus rien à lire.
     */
    public int lire() {
        try {
            int b = lecteur.read();
            if (bitMode && b > 0) {
                b = (b == '0' ? 0 : 1);
            }
            return b;
        } catch(Exception e) {
            return -1;
        }
    }


    
    /**
     * Remet le flux de lecture en début de fichier.
     *
     */
    public void retourDebut() {
        try {
            if (lecteur != null) {
                lecteur.close();
            }
            lecteur = new FileInputStream(nom);
        } catch(Exception e) {
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }


    /**
     * Permet de préciser le mode de lecture.
     * <p>
     * En mode bit, le caractère '0' (48 en ASCII) est lit comme un bit 0, 
     * le caractère '1' (49 en ASCII)est lu comme un 1.
     * Si le mode bit n'est pas actif, la méthode lire renvoie la valeur de
     * l'octet (48 pour '0' et 49 pour '1').
     * </p>
     *
     * @param bitMode
     *        précise le mode de lecture de bit.
     */
    public void setBitMode(boolean bitMode) {
        this.bitMode = bitMode;
    }

    
}
