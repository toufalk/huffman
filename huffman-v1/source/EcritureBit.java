
/**
 * Ecriture de fichiers.
 *
 * Écrit dans le fichier donné dans le constructeur, bit par bit.
 * <p>
 * La méthode de completion d'octet est la méthode de bourrage en fin de fichier.
 * Le nombre de bit de bourrage est indiqué par les 3 premiers bits du fichier.
 * </p>
 * <p>
 * On stocke l'intégralité des données dans un StringBuffer pour connaître 
 * le nombre de bit à bourrer. L'utilisateur doit donc impérativement
 * utiliser la méthode flush() pour valider l'écriture et qu'elle soit effective.
 * </p>
 *
 * @author Vincent
 *
 */
public class EcritureBit implements Ecriture {

    
    private EcritureOctet eo;
    private StringBuffer bigBuff; // le buffer du fichier à écrire
    private int buff; // buff: le buffer d'un octet
    private int nb; // la position d'écriture à l'itérieur de buff.


    /**
     * Création d'un scripteur à partir d'un nom de fichier.
     *
     * @param nomFichier
     *        le nom du fichier à écrire
     */
    public EcritureBit(String nomFichier) {
        eo = new EcritureOctet(nomFichier);
        initBuffer();
    }

    /**
     * Création d'un scripteur de bit à partir d'un scripteur d'octet.
     *
     * @param eo
     *        le flux d'écriture d'octet
     */
    public EcritureBit(EcritureOctet eo) {
        this.eo = eo;
        initBuffer();
    }

    
    // initialisation des buffers et préparation des 3 bits d'indication de bourrage.
    private void initBuffer() {
        bigBuff = new StringBuffer();
        buff = 0;
        nb = 3; // pour réserver de l'espace pour coder le bourrage.
    }

    
    /**
     * Ecriture de l'octet à la fin du fichier.
     * <p>
     * Cette méthode attend un entier bit qui vaut soit 0 soit 1.
     * Toutes les autres valeurs provoqueront un comportement indéterminé.
     * </p>
     *
     * @param bit
     *        le bit à écrire dans le fichier
     */
    public void ecrire(int bit) {
        buff = buff | (bit * (1<<nb));
        nb++;
        if (nb == 8) {// octet plein, on archive et on prépare le suivant.
            bigBuff.append((char)buff);
            buff = 0;
            nb = 0;
        }
    }

    
    /**
     * Ecriture de la chaine de caractères à la fin du fichier.
     * <p>
     * La chaine doit contenir une représentation d'un nombre binaire.
     * Elle écrit caractère par caractère. Si le caractère est un '0', 
     * la méthode écrit un bit à 0; sinon elle écrit un 1.
     * </p>
     *
     * @param texte
     *        la chaine à écrire dans le fichier.
     */
    public void ecrire(String texte) {
        for (int k=0; k<texte.length(); k++) {
            ecrire((texte.charAt(k) == '0' ? 0 : 1));
        }
    }


    /**
     * méthode qui valide l'écriture du fichier.
     *
     * <p>
     * C'est cette méthode qui effectue réellement l'écriture dans le fichier.
     * </p>
     */
    public void flush() {
        if (nb > 0) {
            bigBuff.append((char)buff);
        }
        // codage du bourrage au debut
        int debut = bigBuff.charAt(0) | ((8-nb)%8);
        bigBuff.setCharAt(0, (char)debut);
        // ecriture !!
        for (int k=0; k< bigBuff.length(); k++) {
            eo.ecrire(bigBuff.charAt(k));
        }
        eo.flush();
    }

}
