
/**
 * Un noeud interne de l'arbre de Huffman.
 * <p>
 * Il stocke des références aux fils.
 * Il implémente bien sûr l'interface ArbreH.
 * </p>
 *
 * @author Vincent
 */
public class NoeudH implements ArbreH {


    private ArbreH fg, fd;
    

    /**
     * Construction d'un noeud interne à partir de ses fils.
     *
     * @param fg
     *        le fils gauche du noeud
     * @param fd
     *        le fils droit du noeud
     */
    public NoeudH(ArbreH fg, ArbreH fd) {
        this.fg = fg;
        this.fd = fd;
    }

    
    /*
     * Méthodes de l'interface ArbreH
     */

    /**
     * @return Faux
     */
    public boolean estFeuille() {
        return false;
    }

    public ArbreH filsGauche() throws RuntimeException {
        return fg;
    }

    public ArbreH filsDroit() throws RuntimeException {
        return fd;
    }

    /**
     * Lève une exception.
     */
    public int etiquette() throws RuntimeException {
        throw new RuntimeException("Un noeud interne n'a pas d'étiquette...");
    }

    /**
     * fixe le fils gauche du noeud.
     *
     * @param a
     *        le nouveau fils gauche
     */
    public void setFilsGauche(ArbreH a) {
        fg = a;
    }


    /**
     * fixe le fils droit du noeud.
     *
     * @param a
     *        le nouveau fils droit
     */
    public void setFilsDroit(ArbreH a) {
        fd = a;
    }


    /**
     * Affichage de l'arbre.
     * 
     * @param prefixe
     *        un prefixe à ajouter à chaque début de ligne de l'arbre.
     * @param adroite
     *        Affiche-t-on la partie droite d'un sur-arbre ?
     *
     * @return une chaine représentant l'arbre.
     */
    public String toString(String prefixe, boolean adroite) {
        String prefixegauche, prefixedroite, msg;
        if (adroite) {
            prefixedroite = "    ";
            prefixegauche = "|   ";
        } else {
            prefixegauche = "    ";
            prefixedroite = "|   ";
        }
        msg = filsGauche().toString(prefixe + prefixegauche, false);
        msg += prefixe + "+---+\n";
        msg += filsDroit().toString(prefixe + prefixedroite, true);
        return msg;
    }

    
    public String toString() {
        // On enlève les caractères | de debut de ligne.
        // Sinon, on a l'impression qu'on affiche un sous arbre.
        return toString("", false).replace("\n|", "\n ");
    }
    
}

    
