
/**
 * Élément de la FAP de Huffman.
 * <p>
 * Les éléments de la FAP sont constitués d'un arbre de Huffman et de sa priorité.
 * On veut pouvoir comparer des éléments entre eux pour les organiser dans la FAP.
 * </p>
 *
 * @author Raimondo, Erwan
 */
public class ElementFAP {
    
    public ArbreH arbre;
    public int prio;


    /**
     * Construction d'un élément à partir de ses constituants
     *
     * @param a
     *        l'arbre de Huffman
     * @param p
     *        la priorité de l'élément
     */
    public ElementFAP(ArbreH a, int p) {
	arbre = a;
	prio = p;
    }



    /**
     * méthode de comparaison de priorité.
     *
     * @param elem
     *        l'élément à comparer.
     *
     * @return un entier positif si <i>this</i> est plus prioritaire que <i>elem</i>,
     *         un entier négatif si <i>elem</i> est plus prioritaire que <i>this</i>,
     *         zéro si <i>elem</i> et <i>this</i> sont de même priorité.
     */
    public int compareTo(ElementFAP elem) {
	return elem.prio - prio;
    }



    /**
     * Affichage d'un ElementFAP
     * 
     * @return une chaine de caractère représentant l'ElementFAP.
     */
    public String toString() {
        return "Priorité: " + prio + "\nArbre : " + arbre;
    }
}
