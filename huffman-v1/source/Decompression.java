import java.util.LinkedList;

/**
 * Coeur de la décompression de Huffman.
 * <p>
 * Cette classe ne dispose que de méthodes statiques (pas d'objet).
 * Elle propose la decompression selon Huffman en version statique (la version).
 * La version dynamique de la méthode est prévue.
 * </p>
 *
 * @author Raimondo, Erwan, Vincent
 */
public class Decompression {

   
    /**
     * Décompresse le fichier par la méthode statique de Huffman
     * <p>
     * On commence par lire l'arbre puis on lit bit à bit le fichier compréssé
     * et on se déplace dans l'arbre de Huffman, 0 à gauche, 1 à droite.
     * Quand on tombe sur une feuille, on écrit le caractère correspondant à l'étiquette dans le fichier de sortie
     * et on se replace à la racine.
     * On itère le processus jusqu'à la fin du fichier compressé.
     * Enfin, on sauve le fichier décompressé.
     * </p>
     *
     * @param occurrence
     *        le fichier où se trouve l'arbre à lire
     * @param compresse
     *        le fichier compréssé à lire pour décodage
     * @param sortie
     *        le fichier dans lequel on écrit le texte décodé
     *
     * @see ArbreH
     */
    public static void statique(LectureOctet occurrence, Lecture compresse, EcritureOctet sortie) {
        ArbreH racine = destockArbre(occurrence);
        // passage en mode bit : la lecture ne donne que des 0 ou 1.
        compresse.setBitMode(true);
        ArbreH noeud = racine;
        int lu = compresse.lire();
        while (lu != -1) {
            noeud = (lu == 0 ? noeud.filsGauche() : noeud.filsDroit());
            //if (lu == 0) noeud = noeud.filsGauche();
            //else noeud = noeud.filsDroit();
            if (noeud.estFeuille()) {
                sortie.ecrire(noeud.etiquette());
                //System.out.println("--> " + (char)noeud.etiquette());
                noeud = racine;
            }
            lu = compresse.lire();
        }
        // fin de décompression
        sortie.flush();
    }



    private static ArbreH destockArbre(LectureOctet occ) {
        /*
         * méthode pour appeler la méthode récursive de reconstruction de l'arbre.
         *
         * 1er octet : nb de feuilles
         * Les 2n suivants sont les couples (octet, profondeur).
         *
         * On stocke tout dans une file puis on reconstruit l'arbre.
         */
        int nbcar = occ.lire() + 1;
        ArbreH arbre;
        LinkedList<ElementFAP> liste = new LinkedList<ElementFAP>();
        for (int k=0; k<nbcar; k++) {
            liste.add(new ElementFAP(new FeuilleH(occ.lire()), occ.lire()));
        }
        //System.out.println("taille liste : " + liste.size());
        return destockageArbre(0, liste);
    }


    private static ArbreH destockageArbre(int profondeur, LinkedList<ElementFAP> liste) {
        /*
         * méthode récursive de reconstruction.
         *
         * Si on est à la bonne profondeur, on crée un feuille
         * sinon, on crée un noeud interne qu'on rempli, d'abord à gauche, puis à droite
         * en vidant progressivement la file.
         */
        if (profondeur == liste.getFirst().prio) {
            return liste.poll().arbre;
        }
        NoeudH ne = new NoeudH(null, null);
        ne.setFilsGauche(destockageArbre(profondeur+1, liste));
        ne.setFilsDroit(destockageArbre(profondeur+1, liste));
        return ne;
    }

}
