
/**
 * Réalise l'interface Homme-java pour la (dé)compression de Huffman.
 * <p>
 * Il propose plusieurs version :
 * <ul>
 * <li>un version statique avec fichier d'occurrence à part, écriture sur des octets,</li>
 * <li>une version statique écriture sur des octets,</li>
 * <li>une version statique écriture sur des bits et</li>
 * <li>une version dynamique sur des bits.</li>
 * </ul>
 * </p>
 *
 * @author Vincent Erwan
 */
public class Huffman {



    private static void printHelp() {
            System.out.println("java Huffman help\n");
            System.out.println("java Huffman code-octet source arbre compress");
            System.out.println("java Huffman code-solo source compress");
            System.out.println("java Huffman code-bit source compress");
            System.out.println("java Huffman code-dyn source compress\n");
            System.out.println("java Huffman decode-octet compress decompress arbre");
            System.out.println("java Huffman decode-solo compress decompress");
            System.out.println("java Huffman decode-bit compress decompress");
            System.out.println("java Huffman decode-dyn compress decompress");
    }


    /**
     * La méthode principale.
     *
     * les possibilités offertes sont les suivantes :
     * <ul>
     * <li>java Huffman help</li>
     * <li>java Huffman code-octet source arbre compress</li>
     * <li>java Huffman code-solo source compress</li>
     * <li>java Huffman code-bit source compress</li>
     * <li>java Huffman code-dyn source compress</li>
     * <li>java Huffman decode-octet compress decompress arbre</li>
     * <li>java Huffman decode-solo compress decompress</li>
     * <li>java Huffman decode-bit compress decompress</li>
     * <li>java Huffman decode-dyn compress decompress</li>
     * </ul>
     *
     */
    public static void main(String[] args) {
        try {
            if ("help".equals(args[0])) {
                // aide
                printHelp();
                System.exit(0);
            }
            if ("code-octet".equals(args[0])) {
                // compression de Huffman
                Compression.statique(new LectureOctet(args[1]),
                                     new EcritureOctet(args[2]),
                                     new EcritureOctet(args[3]));
                
            } else if ("decode-octet".equals(args[0])) {
                // decompression de Huffman
                Decompression.statique(new LectureOctet(args[3]),
                                       new LectureOctet(args[1]),
                                       new EcritureOctet(args[2]));
                
            } else if ("code-solo".equals(args[0])) {
                // compression dans un seul fichier.
                // Le fichier d'arbre et celui compressé est le même.
                EcritureOctet eo = new EcritureOctet(args[2]);
                Compression.statique(new LectureOctet(args[1]), eo, eo);
                
            } else if ("decode-solo".equals(args[0])) {
                // décompression dans un seul fichier.
                // Le fichier d'arbre et celui compréssé est le même.
                LectureOctet lo = new LectureOctet(args[1]);
                Decompression.statique(lo, lo, new EcritureOctet(args[2]));
                
            } else if ("code-bit".equals(args[0])) {
                // compression sur des bits
                EcritureOctet eo = new EcritureOctet(args[2]);
                Compression.statique(new LectureOctet(args[1]), eo, new EcritureBit(eo));
                
            } else if ("decode-bit".equals(args[0])) {
                // decompression sur des bits
                LectureOctet lo = new LectureOctet(args[1]);
                Decompression.statique(lo, new LectureBit(lo), new EcritureOctet(args[2]));
                
            } else if ("code-dyn".equals(args[0])) {
                System.out.println("Pas encore implémenté...");
                
            } else if ("decode-dyn".equals(args[0])) {
                System.out.println("Pas encore implémenté...");
                
            } else {            
                System.err.println(" Erreur. 'java Huffman help' pour l'aide.");
            }
        } catch(IndexOutOfBoundsException e) {
            System.err.println("Pas le bon nombre de paramètres...");
            printHelp();
            System.exit(1);
        }
    }


}
