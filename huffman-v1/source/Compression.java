
/**
 * Coeur de la compression de Huffman.
 * <p>
 * Cette classe ne dispose que de méthodes statiques (pas d'objet).
 * Elle propose la compression selon Huffman en version statique (la version).
 * La version dynamique de la méthode est prévue.
 * </p>
 * 
 *
 * @author Vincent Erwan
 */
public class Compression {
    

    /**
     * Compresse le fichier par la méthode statique de Huffman.
     * <p>
     * On calcule les fréquences d'apparition des octets puis 
     * l'arbre de Huffman et enfin la table de correspondance.
     * Il suffit alors de reparcourir la source pour compresser
     * chaque octet.
     * </p> 
     *
     * @param source
     *        le fichier d'origine à compresser
     * @param occurrences
     *        le fichier dans lequel stocker l'arbre
     * @param resultat
     *        le fichier dans lequel stocker les données
     */
    public static void statique(LectureOctet source, EcritureOctet occurrences, Ecriture resultat) {
        long somme = 0; // nb d'octets total dans le fichier
        
        // calcul du tableau des frequences
        int[] freq = new int[256];
        int carac = source.lire();
        while (carac != -1) {
            somme ++;
            freq[carac]++;
            carac = source.lire();
        }
        System.out.println("Nombre d'octets d'origine : " + somme);
        
        // construction de l'arbre et enregistrement puis la table de correspondance
        ArbreH arbre = construitArbre(freq);
        stockArbre(arbre, occurrences);
        String[] tabcod = GenerCod(arbre,"", new String[256]);

        // Calcul du nombre moyen de bit
        long totalong = 0; // taille du fichier de données en bit.
        for (int k=0; k<256; k++) {
            if (tabcod[k] != null) {
                totalong = totalong + freq[k]*tabcod[k].length();
                //System.out.println("caractere : " + (char)k + " code : " + tabcod[k]);
            }
        }
        double longmoycarac = (0.0 + totalong) / (0.0 + somme);
        System.out.println("Nombre moyen de bits par symbole : " + longmoycarac);

        // boucle de compression
        source.retourDebut();
        int lu = source.lire();
        while (lu != -1) {
            resultat.ecrire( tabcod[lu] );
            lu = source.lire();
        }
        // fin d'écriture
        resultat.flush();
    }



    /**
     * Compression dynamique de Huffman.
     * <p>
     * On ajoute 2 octets virtuels, 256 et 257. Ces octets
     * signifient respectivement une nouvelle feuille dans
     * l'arbre et une fin de fichier.
     * </p>
     * <p>
     * Le fichier source est parcouru une seule fois.
     * Initialement, il n'y a que les octets virtuels dans l'arbre.
     * A chaque lecture d'octet, on le code, puis on met à jour la table
     * de fréquence et l'arbre de Huffman. </br>
     * Si l'octet n'est pas déjà dans l'arbre, on envoie le code de l'octet
     * virtuel 256 (nouvelle feuille) puis l'octet lu. Sinon, on envoie simplement
     * son code. En fin de fichier, on envoie le code de l'octet virtuel 257.
     * </p>
     *
     * @param source
     *        le fichier à compresser
     * @param compresse
     *        le fichier compresser à écrire
     */
    public static void dynamique(LectureOctet source, Ecriture compresse) {
        // on prendra comme convention : 256 -> nouvelle feuille ; 257 -> fin de fichier
        int[] freq = new int[258];
        int octet = source.lire();
        ArbreH arbre;
        while (octet != -1) {
            // ???
            octet = source.lire();
        }
    }

    

    /*
     * méthodes privées
     */
    
    // @param freq: tableau des fréquences (nb occurrences)
    private static ArbreH construitArbre(int[] freq) {
        // initialisation de la FAP
        FAP hf = new FAP();
        double ent = 0.0; // pour entropie
        long somme = 0;
        for (int k=0; k<freq.length; k++) {
            if (freq[k] > 0) {
                ent = ent + freq[k] * Math.log(freq[k]);
                somme += freq[k];
                hf.inserer(new ElementFAP(new FeuilleH(k), freq[k]));
                //System.out.println((char)k + " apparait " + freq[k] + " fois.");
            }
        }
        ent = (Math.log(somme) - ent/somme)/Math.log(2);
        System.out.println("Entropie du fichier d'origine : "+ ent);
        // construction de l'arbre
        while (hf.taille() > 1) {
            ElementFAP elem1 = hf.extraire();
            ElementFAP elem2 = hf.extraire();
            hf.inserer(new ElementFAP(new NoeudH(elem1.arbre, elem2.arbre), elem1.prio+elem2.prio));
        }
        ArbreH arbre = hf.extraire().arbre;
        System.out.println(arbre);
        return arbre;
    }
    
    
    private static void stockArbre(ArbreH arbre, EcritureOctet occ) {
        /*
         * méthode pour appeler la méthode récursive de stockage de l'arbre dans un fichier.
         *
         * fichier : 1 octet pour le nombre de feuilles total
         * puis, 2 octets par feuilles : son étiquette + sa profondeur dans l'arbre.
         *
         * On parcours l'arbre "de gauche à droite" et on ajoute de la même façon.
         */ 
        String txt = stockArbre(arbre, 0);
        occ.ecrire((txt.length()/2)-1);
        System.out.println("nb de feuilles : " + (txt.length()/2));
        occ.ecrire(txt);
    }
    
    
    private static String stockArbre(ArbreH arbre, int prof) {
        /*
         * méthode récursive de stockage.
         *
         * Si on est une feuille, on écrit son étiquette et sa profondeur
         * sinon, on écrit toutes les feuilles de gauche, puis toutes celles de droite.
         */        
        if (arbre.estFeuille()) {
            return "" + (char)arbre.etiquette() + (char)prof;
        } else {
            return stockArbre(arbre.filsGauche(), prof+1) + stockArbre(arbre.filsDroit(), prof+1);
        }
    }
    
    
    
    /*
     * Méthode récursive pour générer la table de correspondance.
     */
    private static String[] GenerCod(ArbreH arbre, String code, String[] tabcod){
        if (arbre.estFeuille()){
            tabcod[arbre.etiquette()] = code;
        } else {
            tabcod = GenerCod(arbre.filsGauche(), code + '0', tabcod);
            tabcod = GenerCod(arbre.filsDroit(), code + '1', tabcod);
        }
        return tabcod;
    }
    
}
