
/**
 * Interface de lecture dans un fichier.
 *
 * @author Vincent
 */
public interface Lecture {

    /**
     * Lecture dans le fichier.
     *
     * @return la donnée lue.
     */
    public int lire();

    /**
     * Remet à zéro la lecture. La prochaine lecture aura lieu
     * au début du fichier.
     */
    public void retourDebut();

    /**
     * Passage en mode bit.
     * <p> En mode bit, la lecture d'un caractère '0' (48 en ASCII) lu dans le fichier 
     * sera retourné à 0 de même '1' donnera 1. </br>
     * En mode non-bit, la lecture d'un caractère '0' renverra '0'.
     * </p>
     */
    public void setBitMode(boolean bitMode);
    
}
