
/**
 * Lecture de fichiers.
 * <p>
 * Lit dans le fichier de données, bit par bit.
 * On utilise une mémoire tampon car java ne lit que par octet.
 * </p>
 * <p>
 * On utilise la méthode de bourrage en fin de fichier.
 * Les 3 pemiers bits du fichiers indiquent le nombre de bits de bourrage.
 * Ces 3 bits ne sont pas proposés à la lecture, ils sont internes au
 * fonctionnement des classes de lecture/écriture de bit.
 * La lecture est donc transparente.
 * </p>
 *
 * @author Vincent
 *
 */
public class LectureBit implements Lecture {


    private LectureOctet lo;
    private int buff, buff2; // deux octets de buffer pour détecter le bourrage.
    private int nb; // l'indice de lecture à l'interieur d'un octet.
    private int bourrage; // le nombre de bit de bourrage.
    
    /*
     * buff est le buffer en cours de lecture (octet).
     * buff2 est le prochain octet à lire, juste après buff.
     * De cette façon, on peut detecter la fin de fichier avec buff2 == -1
     * et empecher la lecture des bits de bourrage qui se trouvent dans buff.
     */
        

    /**
     * Création d'un lecteur à partir du nom de fichier.
     *
     * @param nomFichier
     *        le nom du fichier à lire
     */
    public LectureBit(String nomFichier) {
        lo = new LectureOctet(nomFichier);
        setBitMode(true);
    }


    /**
     * Création d'un lecteur de bit à partir d'un lecteur d'octet.
     * <p>
     * Ce flux peut avoir déjà lu des choses.
     * La lecture reprend où elle en était.
     * </p>
     *
     * @param lo
     *        le flux de lecture d'octet.
     */
    public LectureBit(LectureOctet lo) {
        this.lo = lo;
    }

    

    /**
     * Lecture du prochain bit du fichier.
     *
     * @return le prochain bit du fichier ou -1 s'il n'y a plus rien à lire.
     */
    public int lire() {
        if (buff2 == -1 && nb >= bourrage) { // fin de fichier, le reste n'est que bourrage
            return -1;
        }
        if (nb == 8) { // on est arrivé au bout du 1er buffer
            buff = buff2; // on recharge le suivant
            buff2 = lo.lire();
            nb = 0;
        }
        int ret = (buff & (1 << nb)) >> nb; // décalage par masque pour récupérer le bit.
        nb++;
        return ret;
    }
    
    
    /**
     * Remet le flux de lecture en début de fichier.
     *
     */
    public void retourDebut() {
        lo.retourDebut();
        buff = lo.lire();
        buff2 = lo.lire();
        nb = 3;
    }

    /**
     * Passage en mode bit.
     * <p>
     * Si bool est vrai, la méthode initialise la lecture de bit
     * en prenant en compte le bourrage :
     * Les 3 premiers bits de l'octet courrant indiquent
     * le nombre de bits de bourrage en fin de fichiers.
     * </p>
     *
     * @param bool
     *        mode bit actif ou non
     */
    public void setBitMode(boolean bool) {
        if (bool) {
            buff = lo.lire();
            buff2 = lo.lire();
            bourrage = 8 - buff & 7;
            if (bourrage == 0) {
                bourrage = 8;
            }
            nb = 3;
        }
    }

}
