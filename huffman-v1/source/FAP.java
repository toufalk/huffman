
/**
 * File à priorité nécessaire pour construire l'arbre de Huffman.
 *
 * @author Erwan
 */
public class FAP {

    
    private ElementFAP[] tab;
    
    // nombre d'éléments actuel dans la FAP.
    private int n;

    
    /**
     * Construit une FAP vide.
     */
    public FAP() { 
        n = 0;
        tab = new ElementFAP[258]; // il faut au maximum 256 octets + 2 octets virtuels (en dynamique).
    }


    /*
     * Méthodes privées
     */

    // Indice du fils droit
    private int fd(int i) {
        return 2*i+2;
    }

    // Indice du fils gauche
    private int fg(int i) {
        return 2*i+1;
    }

    // Indice du père
    private int pere(int i) {
        return (i-1)/2;
    }

    // Fait remonter un élément dans la FAP s'il est plus prioritaire.
    private void percoler_haut(int i) {
        ElementFAP j;
        while (i > 0 && tab[i].compareTo(tab[pere(i)]) > 0) {
            j=tab[i];
            tab[i]=tab[pere(i)];
            tab[pere(i)]=j;
            i=pere(i);
        }
    }

    // Indice du plus grand fils, s'il existe
    private int PgF (int i) {
        if (fg(i)>=n) {
            return i; // je ne comprend pas cette ligne. On devrait renvoyer -1 plutot, non ?
        } else if (fd(i) >= n || tab[fg(i)].compareTo(tab[fd(i)]) > 0) {
            return fg(i);
        } else {
            return fd(i);
        }
    }

    // Fait redescendre un élément dans la FAP s'il est moins prioritaire.
    private void percoler_bas (int i) {
        ElementFAP j;
        while (tab[i].compareTo(tab[PgF(i)]) < 0){
            j=tab[i];
            int f = PgF(i);
            tab[i]=tab[f];
            tab[f]=j;
            i=f;
        }
    }


    
    /**
     * Donne le nombre d'éléments dans la file.
     *
     * @return le nombre d'éléments dans la file
     */
    public int taille() {
	return n;
    }


    /**
     * Insère l'élément à sa place dans la FAP.
     *
     * @param element
     *        l'élément à insérer dans la file
     */
    public void inserer(ElementFAP element) {
        tab[n] = element;
        percoler_haut(n);
        n++;
    }


    /**
     * Enlève et retourne l'élément de priorité maximale de la FAP.
     *
     * @return l'élément de la file de plus haute priorité.
     *
     * @throws IndexOutOfBoundsException si on tente d'extraire un élément d'une FAP vide.
     */
    public ElementFAP extraire() {
	if (n == 0) throw new IndexOutOfBoundsException("FAP vide");
	ElementFAP tmp = tab[0];
	n--;
	tab[0]=tab[n];
	percoler_bas(0);
	return tmp;
    }



    /**
     * Affichage du contenu de la FAP.
     *
     * @return une chaine représentant l'état de la FAP.
     */
    public String toString() {
        String txt = "Taille de la FAP : " + n;
        for (int k=0; k<n; k++) {
            txt += tab[k] + "\n";
        }
        return txt;
    }
    
}
