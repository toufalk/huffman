
/**
 * Une feuille de l'arbre de Huffman.
 * <p>
 * Elle stocke l'étiquette, c'est-à-dire l'octet (le caractère) représenté sous forme d'int.
 * Elle implémente bien sûr l'interface ArbreH
 * </p>
 *
 * @author Erwan
 */
public class FeuilleH implements ArbreH {
  
    private int etiq;

    /**
     * Construction d'une feuille à partir de son étiquette.
     *
     * @param etiq
     *        l'étiquette de la feuille
     */
    public FeuilleH(int etiq) {
        this.etiq = etiq;
    }

    
    /*
     * Méthodes de l'interface ArbreH
     */

    /**
     * @return Vrai.
     */
    public boolean estFeuille() {
        return true;
    }

    /**
     * Lève une exception.
     */
    public ArbreH filsGauche() throws RuntimeException {
       throw new RuntimeException("Une feuille n'a pas de fils gauche...");
    }

    /**
     * Lève une exception.
     */
    public ArbreH filsDroit() throws RuntimeException {
         throw new RuntimeException("Une feuille n'a pas de fils droit...");
    }

    public int etiquette() throws RuntimeException {
        return etiq;
    }


    /*
     * Méthodes pur affichage correct dans un terminal
     */
    public String toString(String prefixe, boolean adroite) {
        return prefixe + "+-- " + (char)etiquette() + "\n";
    }

    public String toString() {
        return toString("", true);
    }
    

}

    
