import java.io.PrintStream;

/**
 * Ecriture de fichiers.
 *
 * Écrit dans le fichier donné dans le constructeur, octet par octet.
 *
 * @author Vincent
 *
 */
public class EcritureOctet implements Ecriture {


    private PrintStream scripteur;
    private long nbCar; // nombre d'octets écrits.
        

    /**
     * @param nomFichier
     *        le nom du fichier à écrire
     */
    public EcritureOctet(String nomFichier) {
        nbCar = 0;
        try {
            scripteur = new PrintStream(nomFichier);
        } catch(Exception e) {
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }


    /**
     * Ecriture de l'octet à la fin du fichier.
     *
     * @param octet
     *        l'octet à écrire dans le fichier
     */
    public void ecrire(int octet) {
        nbCar++;
        scripteur.write(octet);
    }

    
    /**
     * Ecriture de la chaine de caractères à la fin du fichier.
     *
     * @param texte
     *        la chaine à écrire dans le ficheir
     */
    public void ecrire(String texte) {
        for (int k=0; k<texte.length(); k++) {
            ecrire(texte.charAt(k));
        }
    }


    /**
     * Force l'écriture du fichier.
     * Cette méthode ne doit être appelée qu'en toute fin d'écriture.
     */
    public void flush() {
        scripteur.flush();
        System.out.println("Nombre d'octets écrits : " + nbCar);
    }

}
